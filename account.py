class NegativeBalance(Exception):
    pass

class Account(object):

    def __init__(self, initial_balance=0):
        self.balance = initial_balance

    def add(self, amount):
        self.balance += amount

    def subtract(self, amount):
        if self.balance < amount:
            raise NegativeBalance('Negative Balance not allowed')
        self.balance -= amount

if __name__ == "__main__":
    account = Account()
    account.add(42)
    print(f'The account balance is {account.balance}')
