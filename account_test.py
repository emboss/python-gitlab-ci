import pytest
from account import Account, NegativeBalance

def test_initial_balance_default():
    account = Account()
    assert account.balance == 0

def test_initial_balance_custom():
    account = Account(10)
    assert account.balance == 10

def test_add():
    account = Account()
    account.add(20)
    assert account.balance == 20

def test_subtract():
    account = Account(100)
    account.subtract(40)
    assert account.balance == 60

def test_negative_balance_when_zero():
    account = Account()
    with pytest.raises(NegativeBalance):
        account.subtract(1)

def test_negative_balance_when_non_zero():
    account = Account(10)
    with pytest.raises(NegativeBalance):
        account.subtract(11)




