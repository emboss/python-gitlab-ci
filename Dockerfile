FROM python:3.8-alpine

ENV APP_ROOT /app
RUN mkdir -p $APP_ROOT

# Copy requirements.txt and run pip install
COPY requirements.txt $APP_ROOT/
WORKDIR $APP_ROOT
RUN pip install -r requirements.txt

# Copy sources to $APP_ROOT
COPY . .

# Change ownership of all files
RUN chown -R nobody:nogroup .
USER nobody

# Default command to be run
CMD ["docker/run.sh"]

